# frozen_string_literal: true

Rails.application.routes.draw do
  # BEGIN
  root 'home#index'
  resources :articles, only: %i[index show]
  # END
end
