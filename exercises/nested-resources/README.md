# Nested resources

## Ссылки

* [Nested Resources routing](https://guides.rubyonrails.org/routing.html#nested-resources)
* [The Types of Associations](https://guides.rubyonrails.org/association_basics.html#the-types-of-associations)

## Задачи

### CRUD

Создайте CRUD комментария с указанием родительской модели `PostComment`. Связанные комментарии отображаются списком на странице просмотра поста. У каждого комментария есть ссылка для редактирования и для удаления (с подтверждением). Отдельных страниц просмотра комментариев списком нет.

### exercises/nested-resources/app/models/post.rb

Добавьте в модель Post связь с комментариями.

### config/routes.rb

Напишите ресурсные роуты для поста и для вложенного ресурса комментария.

### app/views/posts/show.html.slim

Добавьте на страницу просмотра поста форму создания комментариев и список созданных под этим постов комментариев. У каждого комментария есть ссылка для удаления. При нажатии на ссылку появляется подтверждение.

### Подсказки

* Иерархию моделей и контроллеров можно подглядеть в [hexlet-basics](https://github.com/hexlet-basics/hexlet-basics)
