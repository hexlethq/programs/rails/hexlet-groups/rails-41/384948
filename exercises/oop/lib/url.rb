# frozen_string_literal: true

require 'forwardable'
require 'uri'

# BEGIN

# main class
class Url
  extend Forwardable
  include Comparable

  def initialize(url_string)
    @uri = URI(url_string)
  end

  def_delegators :@uri, :host, :scheme, :to_s

  def query_params
    @query_params ||= parse_query_params
  end

  def query_param(key, default_value = nil)
    query_params.fetch(key, default_value)
  end

  private

  attr_reader :uri

  def <=>(other)
    to_s <=> other.to_s
  end

  def parse_query_params
    uri.query.split('&').each_with_object({}) do |string, acc|
      key, value = string.split('=')
      acc[key.to_sym] = value
    end
  end
end
# END
