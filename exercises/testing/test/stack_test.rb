# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'
# testing class
class StackTest < Minitest::Test
  # BEGIN
  def test_pop!
    stack = Stack.new(%w[one two three])
    assert_equal stack.size, 3
    value = stack.pop!

    assert(value == 'three')
    assert(stack.to_a == %w[one two])
    assert(stack.size == 2)
  end

  def test_push!
    stack = Stack.new %w[one two three]
    stack.push! 'four'
    stack.push! 'five'
    stack.push! 'six'

    assert(stack.to_a == %w[one two three four five six])
    assert(stack.size == 6)
  end

  def test_empty?
    stack = Stack.new
    stack.empty?

    assert(stack.empty? == true)
    assert_equal stack.size, 0
  end

  def test_to_a
    stack = Stack.new %w[one two]
    value = stack.to_a

    assert(value == %w[one two])
  end

  def test_clear
    stack = Stack.new %w[one two three]
    value = stack.clear!

    assert_equal true, value.empty?
  end

  def test_size
    stack = Stack.new %w[one two three four]
    value = stack.size

    assert(value == 4)
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
