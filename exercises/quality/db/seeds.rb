# frozen_string_literal: true
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
5.times do |index|
  note_index = index + 1
  Bulletin.create(title:"Bullet title #{note_index}", body:"I am number #{note_index}", published: true)
end

