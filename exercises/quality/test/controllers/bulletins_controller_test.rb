class BulletinsControllerTest < ActionDispatch::IntegrationTest
  test 'opens index page with bulletins' do
    get bulletins_path

    assert_response :success
    assert_select 'h1', 'Bulletins'
    assert_select 'a', 'Title 1'
    assert_select 'a', 'Title 2'
  end
  test 'opens show page with first bulletin' do
    get bulletin_path(bulletins(:first))
    
    assert_response :success
    assert_select 'h3', 'Title 1'
    assert_select 'p', 'Description 1'
  end

end