require "test_helper"

class TasksFlowTest < ActionDispatch::IntegrationTest
  test 'open all tasks page' do
    get tasks_path
    assert_response :success
    assert_select 'h1', 'Task'
  end

  test 'can create task' do
    get '/tasks/new'
    assert_response :success
    post "/tasks",
      params: { task: { name: "Fry pork", description: "successfully" } }
    assert_response :success
  end

end
