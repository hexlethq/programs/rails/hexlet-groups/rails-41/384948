require "test_helper"

class TasksControllerTest < ActionDispatch::IntegrationTest
  test 'opens index page' do
    get tasks_path
    assert_response :success
    assert_select 'h1', 'Task'
  end
  
  test 'opens show page' do
    get task_path(tasks(:one))
    assert_response :success
    assert_select 'h1', "Task # #{tasks(:one).id}"
    assert_select 'p', 'Name: MyString'
  end

  test 'creates new task if validation passes' do
    get new_task_path
    assert_response :success

    post tasks_path, params: { task: {
      name: 'name_str',
      description: 'description_text',
      status: 'status_str',
      creator: 'creator_str',
      performer: 'performer_str',
      completed: true
    } }
    assert_redirected_to task_path(Task.last)
    follow_redirect!
    assert_response :success
    #puts @response.body
    assert_select 'p', 'Name: name_str'
  end

  test 'displays errors on new page if validation fails' do
    get new_task_path
    assert_response :success
    post tasks_path, params: { task: { description: 'MyString3' } }
    assert_select 'div', "Name can't be blank"
  end

  test "update task" do
    patch task_path(tasks(:two)), params: { task: { name: "Task rename" }  }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_select 'h1', 'Task # 298486374'
  end



  test 'deletes existing task' do
    get task_path(tasks(:two))
    assert_response :success

    delete task_path(tasks(:two))
    assert_redirected_to tasks_path
    follow_redirect!
    assert_response :success
    assert_select 'li', { count: 0, text: 'MyString2' }
  end

end
