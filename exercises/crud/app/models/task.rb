class Task < ApplicationRecord
  validates :name, :status, :creator, presence: true
  # validates :name, :status, :creator, :completed,  format: { with: /\A[a-zA-Z _]+\z/,
  #   message: "only allows letters" }
end
