# frozen_string_literal: true

# BEGIN
def get_same_parity(array)
  return array if array.empty?
  return array.select(&:even?) if array[0].even?
  return array.reject(&:even?) unless array[0].even?
end
# END
