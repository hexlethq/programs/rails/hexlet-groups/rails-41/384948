# frozen_string_literal: true

# BEGIN
def anagramm_filter(word, anagramms)
  anagramms.filter { |item| word.chars.sort.to_s == item.chars.sort.to_s }
end
# END
