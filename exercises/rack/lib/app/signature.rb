require 'digest'

class Signature
  def initialize(app)
    @app = app
  end

  def call(env)
    status, _, body = @app.call(env)
    if status == 200
      sha256body = Digest::SHA256.hexdigest body
      [200, {}, "#{body}\n#{sha256body}"]
    else
      @app.call(env)
    end
  end
end
