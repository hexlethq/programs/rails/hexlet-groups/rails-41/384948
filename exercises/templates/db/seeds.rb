10.times do |index|
  Task.create(
    name: Faker::Food.dish,
    description: Faker::Food.description,
    status: 'completed',
    creator: Faker::Name.unique.name,
    performer: Faker::Name.unique.name,
    completed: Faker::Boolean.boolean
  )
  end