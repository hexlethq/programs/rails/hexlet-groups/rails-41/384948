# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  return nil if start > stop
  array = []
  (start..stop).each do |element|
    if (element % 3).zero? && (element % 5).zero?
      array << 'FizzBuzz'
    elsif (element % 3).zero?
      array << 'Fizz'
    elsif (element % 5).zero?
      array << 'Buzz'
    else
      array << element
    end
  end
  array.join(' ')
end
# END