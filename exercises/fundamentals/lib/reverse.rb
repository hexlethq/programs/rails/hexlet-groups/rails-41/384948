# frozen_string_literal: true

# BEGIN
def reverse(word)
  result = []
  array = word.chars
  array.each { |c| result.unshift(c) }
  result.join('')
end
# END