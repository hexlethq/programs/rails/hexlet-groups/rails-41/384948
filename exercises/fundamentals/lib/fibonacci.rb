# frozen_string_literal: true

# BEGIN
def fibonacci(num)
  return nil unless num.positive?
  return num - 1 if num <= 2

  fibonacci(num - 1) + fibonacci(num - 2)
end
# END

p fibonacci(10)
