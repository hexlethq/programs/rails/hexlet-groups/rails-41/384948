# frozen_string_literal: true

# BEGIN
def compare_versions(version1, version2)
  ver1_split = version1.split('.').map(&:to_i)
  ver2_split = version2.split('.').map(&:to_i)
  ver1_split <=> ver2_split
end
# END
