# frozen_string_literal: true

# BEGIN
def build_query_string(params)
  query = params.sort.map do |key, value|
    "#{key}=#{value}"
  end
  query.join('&')
end
# END
