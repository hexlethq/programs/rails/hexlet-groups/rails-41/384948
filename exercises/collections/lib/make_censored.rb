# frozen_string_literal: true

# BEGIN
def make_censored(text, stop_words)
  text.split(' ').map do |word|
    stop_words.include?(word) ? '$#%!' : word
  end.join(' ')
end
# END
