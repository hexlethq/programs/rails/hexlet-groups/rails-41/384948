# config/environments/*.rb
Rails.application.configure do
  # Задаём минимальный уровень логирования
  # https://guides.hexlet.io/logging
  # https://guides.rubyonrails.org/debugging_rails_applications.html
  config.log_level = :info
end
